#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb@libreapps.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import cherrypy
import couchdb
import datetime
couch = couchdb.Server("http://127.0.0.1:5984/")
db = couch["gmpte"]

"""

{
   "_id": "_design/test",
   "language": "javascript",
   "views": {
       "routes": {
           "map": "function(doc) { if (doc.type == 'route') { emit(doc.service_number, doc); } }"
       },
       "route_journeys": {
           "map": "function(doc) { if (doc.type == 'journey') { emit(doc.route, doc); } }"
       },
       "journey_stoptimes": {
           "map": "function(doc) { if (doc.type == 'stoptime') { emit(doc.journey, doc) } }"
       }
   }
}

"""

def withindate(doc):
    today = datetime.date.today()
    s = datetime.datetime.strptime(doc["data_start_date"], "%Y%m%d").date()
    if doc["data_end_date"] == "00000000":
        return s <= today
    else:
        e = datetime.datetime.strptime(doc["data_end_date"], "%Y%m%d").date()
        return s <= today and today <= e

class WebApp(object):
    def index(self):        
        out = "<ol>"
        results = db.view("test/routes")
        for result in results:
            print result["value"]
            out += "<li><a href=\"/route/"+result["key"]+"\">"+result["key"]+"</a></li>"
        out += "</ol>"
        return out
        
    def route(self, num):
        if len(num) > 4: return "Invalid route number."
        out = ""
        id = 0
        results = db.view("test/routes", key=num)
        for result in results:
            doc = result["value"]
            id = doc["_id"]
            for (k,v) in doc.items():
                out += "<strong>"+k+":</strong> "+v+"<br/>"
            break
        
        out += "<br/><br/>"
        
        results = db.view("test/route_journeys", key=id)
        i=0
        for result in results:
            doc = result["value"]
            if withindate(doc):
                i+=1
                out += "<strong>"+str(i)+"</strong><br/>"
                for (k,v) in doc.items():
                    if k == "_id":
                        out += "<strong>"+k+":</strong> <a href=\"/journey/"+str(v)+"\">"+str(v)+"</a><br/>"
                    else:
                        out += "<strong>"+k+":</strong> "+str(v)+"<br/>"
                out += "<br/>"
        
        return out
    
    def journey(self, id):
        doc = db[id]
        out = ""
        for (k,v) in doc.items():
            out += "<strong>"+k+":</strong> "+str(v)+"<br/>"
        out += "<br/><br/>"
        
        results = db.view("test/journey_stoptimes", key=id)
        i=0
        from xml.dom.minidom import parse
        import urllib2
        for result in results:
            doc = result["value"]
            i+=1
            out += "<strong>"+str(i)+"</strong><br/>"
            for (k,v) in doc.items():
                out += "<strong>"+k+":</strong> "+str(v)+"<br/>"
            
            """    
            if doc["location"] in db:
                stop = db[doc["location"]]
            else:
                xml = parse(urllib2.urlopen(" http://www.informationfreeway.org/api/0.6/node[naptan:AtcoCode="+doc["location"]+"]"))
                node = xml.getElementsByTagName("node")[0]
                stop = {}
                stop["openstreetmapid"] = node.attributes["id"].value
                stop["lat"] = node.attributes["lat"].value
                stop["lon"] = node.attributes["lon"].value
                for tag in node.getElementsByTagName("tag"):
                    stop[tag.attributes["k"].value] = tag.attributes["v"].value
                db[stop["naptan:AtcoCode"]] = stop
            for k in ["lat", "lon"]:
                out += "<strong>"+k+":</strong> "+str(stop[k])+"<br/>"
            """
            #break
                
            out += "<br/>"
        return out
    
    index.exposed = True
    route.exposed = True
    journey.exposed = True
    
config = {
    "global": {
        "server.socket_port": 8010
    }
}
    
cherrypy.quickstart(WebApp(), config=config)
