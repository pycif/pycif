#!/usr/bin/env python
# Copyright (c) 2010 Ben Webb <bjwebb@libreapps.com>
# Released as free software under the MIT license,
# see the LICENSE file for details.
import os
from uuid import uuid4
import couchdb
import datetime
couch = couchdb.Server("http://127.0.0.1:5984/")
db = couch["gmpte"]

def withindate(sd, ed):
    today = datetime.date.today()
    s = datetime.datetime.strptime(sd, "%Y%m%d").date()
    if ed == "00000000":
        return s <= today
    else:
        e = datetime.datetime.strptime(ed, "%Y%m%d").date()
        return s <= today and today <= e

date = "20100726"
dir = os.path.join("GMPTE_CIF_"+date)
for f in os.listdir(dir):
    print f
    if f == "GM___17_.CIF":
        continue
    file = open(os.path.join(dir,f))
    route = {}
    header = file.readline()
    fields = {
        "file_type": (8, 1),
        "version_major": (2, 9),
        "version_minor": (2, 11),
        "file_originator": (32, 13),
        "source_product": (16, 45),
        "production_date": (8, 61),
        "production_time": (6, 69)
    }
    for (field, (length, start)) in fields.items():
        route[field] = header[start-1:start+length-1].strip()
    print route
    
    journey = None
    journeyid = None
    start_date = None
    end_date = None
    broken = False
    checked = False
    input = False
    docs = []
    for line in file:
        #print start_date, end_date
        code = line[:2]
        #print code
        if code == "ZD":
            start_date = line[2:10]
            end_date = line[10:18]
            if withindate(start_date, end_date):
                input = True
            else:
                input = False
        elif code == "ZS":
            fields = {
                "service_reference": (8, 3),
                "service_number": (4, 11), #(4, 12), IS THIS AN ERROR IN THE SPEC? FIXME
                "description": (50, 15)
            }
            for (field, (length, start)) in fields.items():
                route[field] = line[start-1:start+length-1].strip()
            if not checked:
                if route["service_reference"] in db:
                    broken = True
                    break
                else:
                    checked = True
        elif input and code == "QS":
            fields = {
                "transaction_type": (1, 3),
                "operator": (4, 4),
                "id": (6, 8),
                "first_date": (8, 14),
                "last_date": (8, 22),
                "term_time": (1, 37),
                "bank_holidays": (1, 38),
                "route_number": (4, 39),
                "running_board": (6, 43),
                "vehicle_type": (8, 49),
                "registration_number": (8, 57),
                "route_direction": (1, 65)
            }
            journey = {}
            for (field, (length, start)) in fields.items():
                journey[field] = line[start-1:start+length-1].strip()
            journey["days"] = []
            for i in range(30,37):
                journey["days"].append(line[i-1:i])
            journey["route"] = route["service_reference"]
            journey["type"] = "journey"
            journey["data_start_date"] = start_date
            journey["data_end_date"] = end_date
            journeyid = uuid4().hex
            journey["_id"] = journeyid
            docs.append(journey)
        elif input and (code == "QO" or code == "QI" or code == "QT"):
            if code == "QO":
                fields = {
                    "location": (12, 3),
                    "published_departure_time": (4, 15),
                    "bay_number": (3, 19),
                    "T": (2, 22),
                    "F": (2, 24)
                }
            elif code == "QI":
                fields = {
                    "location": (12, 3),
                    "published_arrival_time": (4, 15),
                    "published_departure_time": (4, 19),
                    "activity_flag": (1, 23),
                    "bay_number": (3, 24),
                    "T": (2, 27),
                    "F": (2, 29)
                }
            elif code == "QT":
                fields = {
                    "location": (12, 3),
                    "published_arrival_time": (4, 15),
                    "bay_number": (3, 19),
                    "T": (2, 22),
                    "F": (2, 24)
                }
            stoptime = {}
            for (field, (length, start)) in fields.items():
                if field == "F":
                    stoptime["fare_stage"] = (line[start-1:start+length-1] == "F1")
                elif field == "T":
                    stoptime["timing_point"] = (line[start-1:start+length-1] == "T1")
                else:
                    stoptime[field] = line[start-1:start+length-1].strip()
            stoptime["journey"] = journeyid
            stoptime["type"] = "stoptime"
            stoptime["data_start_date"] = start_date
            stoptime["data_end_date"] = end_date
            stoptime["_id"] = uuid4().hex
            docs.append(stoptime)
    if broken: continue
    route["type"] = "route"
    route["_id"] = route["service_reference"]
    docs.append(route)
    db.update(docs)
    #break
